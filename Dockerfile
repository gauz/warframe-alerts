FROM node:alpine as build-fe
WORKDIR /build
ADD public /build
RUN npm run build

FROM golang:latest as build-be 
WORKDIR /build
ADD . /build
RUN make build_prod

FROM alpine:latest
WORKDIR /app
COPY --from=build-fe /build/dist /app/dist
COPY --from=build-be /build/exec/warframe-alerts /app/
CMD ["./warframe-alerts"]
