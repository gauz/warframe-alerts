module gitlab.com/gauz/warframe-alerts

require (
	github.com/gorilla/handlers v1.4.0
	github.com/lib/pq v1.0.0
	golang.org/x/net v0.0.0-20181129055619-fae4c4e3ad76
	golang.org/x/text v0.3.0 // indirect
)

go 1.13
