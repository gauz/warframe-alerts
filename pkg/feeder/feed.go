package feeder

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

type DB interface {
	prepare()
	insert()
	get()
}

func prepare() *sql.DB {
	PGUSER := "warframe_alerts_user"
	PGPASS := "warframe_alerts_password"
	PGHOST := "localhost"
	PGDB := "warframe_alerts"
	connStr := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", PGUSER, PGPASS, PGHOST, PGDB)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Printf("Could not open db with err: %v", err)
	}
	return db
}

func insert(db *sql.DB) error {
	query := `INSERT INTO rss (
		guid,
		title,
		author,
		description,
		pubdate,
		faction,
		expiry
		) VALUES($1,$2,$3,$4,$5,$6,$7,$8)`
	return nil
}

func get(db *sql.DB) error {

	return nil
}
