const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Warframe Alerts',
      template: './src/static/index.html'
    }),
    new HtmlWebpackPlugin({
      title: 'Warframe Mods',
      filename: 'mods.html',
      template: './src/static/mods.html'
    }),
    new MiniCssExtractPlugin(),
  ],
  entry: [
    './src/index.js',
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[contenthash].js',
    clean: true,
  },
};

