package routes

import (
	"encoding/json"
	"gitlab.com/gauz/warframe-alerts/pkg/rssparser"
	"log"
	"net/http"
)

type rss struct {
	Guid        string `json:"guid"`
	Title       string `json:"title"`
	Author      string `json:"author"`
	Description string `json:"description,omitempty"`
	Pubdate     string `json:"pubDate,"`
	Faction     string `json:"faction,omitempty"`
	Expiry      string `json:"expiry,omitempty"`
}

// WarframeFeed handles rss feeds from for e.g http://content.warframe.com/dynamic/rss.php
func WarframeFeed(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		url := "http://content.warframe.com/dynamic/rss.php"
		resp, err := http.Get(url)
		if err != nil {
			log.Println(err.Error())
		}
		defer resp.Body.Close()
		feed, err := rssparser.Parse(resp.Body)
		if err != nil {
			log.Println(err.Error())
		}
		json, err := json.Marshal(&feed.Channel.Item)
		w.Header().Set("Content-Type", "application/json")
		w.Write(json)
	default:
		w.WriteHeader(http.StatusForbidden)
	}
}
