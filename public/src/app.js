'use strict';
import moment from 'moment';
let getFeed = async () => {
    const req = await fetch('./api/v1/feed');
    const data = await req.json();
    let htmlData = '';
    for (let i = 0; i < data.length; i++) {
      if (typeof data[i].description != 'undefined') {
        var alertInfo = `<p>${data[i].type}: ${data[i].description}</p>`;
      } else {
        alertInfo = `<p>Type: ${data[i].type}</p>`;
      }
      let expiryData = '';
	  let expiryLocal = parseTime(data[i].pubDate);
      expiryData = `<p>Expires: ${expiryLocal}</p>`;
      htmlData += `<div><h3><p>${data[i].title}</h3>${alertInfo}${expiryData}</div>`;
    }
    document.querySelector('.app').innerHTML = htmlData;
};
function parseTime(time){
  return moment(time).format('YYYY-MM-DD HH:mm');
}
if (document.querySelector('.app')) {
  getFeed();
}
