package rssparser

import (
	"encoding/xml"
	"golang.org/x/net/html/charset"
	"io"
)

// Dates from Pubdate and Expiry are in RFC1123Z
type item struct {
	Guid        string `xml:"guid" json:"guid"`
	Title       string `xml:"title" json:"title"`
	Author      string `xml:"author" json:"type"`
	Description string `xml:"description" json:"description,omitempty"`
	Pubdate     string `xml:"pubDate" json:"pubDate"`
	Faction     string `xml:"faction" json:"faction,omitempty"`
	Expiry      string `xml:"expiry" json:"expiry,omitempty"`
}

type channel struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	Language    string `xml:"language"`
	Item        []item `xml:"item"`
}
type rss struct {
	Channel channel `xml:"channel"`
}

func Parse(r io.Reader) (f rss, err error) {
	decoder := xml.NewDecoder(r)
	decoder.CharsetReader = charset.NewReaderLabel
	if err := decoder.Decode(&f); err != nil {
		return rss{}, err
	}
	return f, nil
}
