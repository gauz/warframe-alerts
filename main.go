package main

import (
	"log"
	"net/http"
	"os"

	"fmt"

	"github.com/gorilla/handlers"
	"gitlab.com/gauz/warframe-alerts/pkg/routes"
)

const port = ":3000"

func main() {
	rootDir := http.FileServer(http.Dir("./dist/"))
	router := http.NewServeMux()
	router.Handle("/", rootDir)
	router.HandleFunc("/mods", routes.WarframeMods)
	router.HandleFunc("/api/v1/feed", routes.WarframeFeed)
	fmt.Println("running")

	log.Println("Listening... on", port)
	log.Println(http.ListenAndServe(port, handlers.LoggingHandler(os.Stdout, router)))

}
