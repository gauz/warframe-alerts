\c template1 postgres
DROP DATABASE IF EXISTS warframe_alerts;
DROP ROLE IF EXISTS warframe_alerts_user;
CREATE USER warframe_alerts_user WITH PASSWORD 'warframe_alerts_password';
CREATE DATABASE warframe_alerts WITH OWNER = warframe_alerts_user;

\c warframe_alerts warframe_alerts_user;

CREATE TABLE IF NOT EXISTS rss (
	id BIGSERIAL PRIMARY KEY,
	guid TEXT,
	title TEXT,
	author TEXT,
	description TEXT,
	pubdate TEXT,
	faction TEXT,
	expiry	TEXT,
	created timestamp without time zone DEFAULT (now() at time zone 'utc')
);

CREATE INDEX idx_faction ON rss (faction);
CREATE INDEX idx_expiry ON rss (expiry);
CREATE INDEX idx_pubdate ON rss (pubdate);
