PACKAGE = warframe-alerts
REV := $(shell git rev-parse --abbrev-ref HEAD)

RUNTIME := docker

.PHONY: clean
clean:
	rm -rf ./exec/*
test_unit: build
	@echo Running unit tests
test_api: build
	@echo Testing api
build_local: clean
	cd public && npm run build
	cp -r ./public/dist ./exec/
	go build -o ./exec/${PACKAGE} main.go
build_prod: clean
	CGO_ENABLED=0 go build -o ./exec/${PACKAGE} -ldflags="-s -w" main.go
run_local:
	cd public && go run ../main.go &
	cd public && npm run test
build:
	${RUNTIME} build -t ${PACKAGE} .
run: build
	${RUNTIME} run --rm -d -p 3000:3000 ${PACKAGE}
publish:
	${RUNTIME} tag ${PACKAGE}:latest registry.gitlab.com/gauz/${PACKAGE}:latest
	${RUNTIME} tag registry.gitlab.com/gauz/${PACKAGE}:latest registry.gitlab.com/gauz/${PACKAGE}:${REV}
	${RUNTIME} push registry.gitlab.com/gauz/${PACKAGE}:${REV}
	${RUNTIME} push registry.gitlab.com/gauz/${PACKAGE}:latest
