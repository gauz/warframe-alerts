package routes

import (
	"io/ioutil"
	"net/http"
)

// func WarframeMods(w http.ResponseWriter, r *http.Request) {
// 	f, _ := ioutil.ReadFile("./public/dist/mods.html")
// 	fmt.Fprintf(w, string(f))
// }

func WarframeMods(w http.ResponseWriter, r *http.Request) {
	f, _ := ioutil.ReadFile("./dist/mods.html")
	w.Write(f)
}
